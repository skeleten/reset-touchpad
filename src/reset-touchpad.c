/**
 * reset-touchpad - Resets the touchpad and trackpoint state
 *
 * This is a very simple file that acquires root priviledges by setting
 * uid and gid, then unloads and reloads the `psmouse` module.
 *
 * Author: 
 *	Jan Pelle Thomson <me@skeleten.me>
 */

#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/**
 * A simple macro to make error-handling much easier.
 *
 * If the action `X` fails, it prints out the message as well as the error
 * string, and exists with the error code produced by the action.
 */
#define TRY(X, MSG) {						\
		     if (X) {					\
			     int errsv = errno;			\
			     fprintf(stderr,			\
				     "Failed to %s: %s\n",	\
				     (MSG),			\
				     strerror(errsv));		\
			     exit (errsv);			\
		     }						\
  }

const uid_t ROOT_UID = 0;
const gid_t ROOT_GID = 0;

/** Print the help/usage */
int help(char* argline);

int
main(int argc, char** argv) {
  /*
   * since we don't parse or use any arguments, supplying some indicates
   * a misuse.
   */
  if (argc > 1) {    
    return help(argv[0]);
  }

  /* 
   * we save uid+gid so we can restore them later.  although it really
   * shouldn't be necessary, since we exit right after.
   */
  uid_t svuid = getuid(), sveuid = geteuid();
  gid_t svgid = getgid(), svegid = getegid();

  /* acquire root privileges */
  TRY(setreuid(ROOT_UID, ROOT_UID), "set UID");
  TRY(setregid(ROOT_GID, ROOT_GID), "set GID");
  
  /* reset our mouse module */
  TRY(system("/usr/bin/rmmod psmouse"), "remove `psmouse` module");
  TRY(system("/usr/bin/modprobe psmouse"), "insert `psmouse` module");

  /* restore uid/gid's */
  TRY(setreuid(svuid, sveuid), "restore UID");
  TRY(setregid(svgid, svegid), "restore GID");

  return 0;
}

int
help(char* argline) {
  printf("USAGE: %s\n", argline);
  printf("Resets the touchpad and trackpoint state\n");

  return 0;
}
