.POSIX:
.SUFFIXES:

CC=clang
CFLAGS=-W -O
LD=clang
LDFLAGS=
LDLIBS=

PREFIX=/usr/local/.

BIN=reset-touchpad
SRC=$(wildcard src/*.c)
OBJ=$(SRC:src/%.c=obj/%.o)

all: $(BIN)

$(BIN): $(OBJ)
	$(LD) $(LDFLAGS) $(LDFLAGS_EXTRA) $(OBJ) -o $(BIN)

obj/%.o: src/%.c
	$(CC) $(CFLAGS) $(CFLAGS_EXTRA) -c $< -o $@

.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(BIN)

.PHONY: install
install: $(BIN)
	install -Dm 4755 $(BIN) $(PREFIX)/bin/$(BIN)
	install -Dm 755 doc/reset-touchpad.1 $(PREFIX)/share/man/man1/

.PHONY: uninstall
uninstall:
	rm -f $(PREFIX)/bin/$(BIN)
	rm -f $(PREFIX)/share/man/man1/reset-touchpad.1
